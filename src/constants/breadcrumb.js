export const BREADCRUMB = [
  {
    id: 1,
    title: "Настройки сайта",
    url: {
      link: "#",
      title: "Перейти к настройкам сайта"
    }
  },
  {
    id: 2,
    title: "Доступ к сайту",
    isActive: true
  }
];
